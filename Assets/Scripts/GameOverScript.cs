﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// Start or quit the game

public class GameOverScript : MonoBehaviour
{
	public GameObject move_left;
	public GameObject move_right;
	public GameObject jump;
	public GameObject crouch;
	public GameObject block;
	public GameObject throw_;
	public GameObject muteBtn;
	public Sprite muteOn;
	public Sprite DisabledResume;
	public Sprite muteOff;
	public Camera Camera;

	public bool paused;
	public bool dead;
	private Button[] buttons;
	public bool mute;
    

    void Awake()
    {
        // Get the buttons
        buttons = GetComponentsInChildren<Button>();
        paused = false;
		mute = false;
		HideButtons();
    }

	public void HideButtons()
    {
        foreach (var b in buttons)
        {
            b.gameObject.SetActive(false);
        }
	}

	public bool PlayerButtons(bool paused)
	{
		move_left.gameObject.SetActive(paused);
		move_right.gameObject.SetActive(paused);
		jump.gameObject.SetActive(paused);
		crouch.gameObject.SetActive(paused);
		block.gameObject.SetActive(paused);
		throw_.gameObject.SetActive(paused);
		muteBtn.gameObject.SetActive(paused);

		return paused;
	}

    public void ShowButtons()
    {
        foreach (var b in buttons)
        {
            b.gameObject.SetActive(true);
			if(dead && b.gameObject.name == "Resume")
			{
				b.GetComponent<Image>().sprite = DisabledResume;
			}
        }

	}

    public void ExitToMenu()
    {
        Time.timeScale = 1;
        Debug.Log("Exit to Menu");
		SceneManager.LoadScene("Menu");

	}

    public void Resume()
    {
		if (paused) //If game is in paused state then change pause state and hidebuttons
        {
            paused = false;
            Debug.Log("Game paused...unpause");
            Time.timeScale = 1;
            HideButtons();
			PlayerButtons(true);
		} 
    }

    public void RestartGame()
    {
        //Make sure game is not in paused state, if in paused state unpause
        //paused = false;
        Time.timeScale = 1; 

        // Reload the level
        Debug.Log("Restart Game");
		SceneManager.LoadScene("Genesis");
	}

	public void mute_sound()
	{
		mute = !mute; //If mute then unmute, if unmute then mute. Negates the current mute state

		if (mute)
		{
			Camera.gameObject.GetComponent<AudioSource>().Pause();
			muteBtn.GetComponent<Image>().sprite = muteOn;
		}

		else
		{
			Camera.gameObject.GetComponent<AudioSource>().Play();
			muteBtn.GetComponent<Image>().sprite = muteOff;
		}
	}
    
    public void Pause()
    {
        paused = !paused;

        if (paused)
        {
            Debug.Log("Game paused!");
            Time.timeScale = 0;
            ShowButtons();
			PlayerButtons(false);
			Camera.gameObject.GetComponent<AudioSource>().Pause();
		}
        else
        {
            Debug.Log("Game resumed!");
            Time.timeScale = 1;
            HideButtons();
			PlayerButtons(true);
			Camera.gameObject.GetComponent<AudioSource>().Play();
		}
    } 

}