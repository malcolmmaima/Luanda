﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

//AND IN THE BEGINING THERE WAS NOTHIN, AND NOTHING WAS BEAUTIFUL. 
//AND SO MALCOLM MAIMA EMBARKED ON A JOURNEY OF PAINFUL CODING, TO BRING YOU AWESOMENESS

	//Bugs to fix
	/* 
	 * Jumping mechanism (may need to also have a change in frame animation from the animator)
	 * Animation for the above have already been setup
	 */

//Implemented Double click mechanism for running. Refer to: http://aidtech-game.com/double-tap-button-unity3d/#.Wror1n-EbIU

public class PlayerController2D : MonoBehaviour
{
	public int moveSpeed;
	public int runningSpeed;
	public int jumpHeight;
	public int extraJumps;
	public int currentPlayerHealth;
	public int maxPlayerHealth;
	public int highscore;
	public int currentScore;
	public int jump_click_count = 0; //For the jump functionanlity Jump()
	public Image healthBar;
	public Text HealthScore;
	public Text PlayerScore;
	public Text Popup;
	public GameObject graphics;
	public Transform groundPoint;
	public GameObject playerFollow;
	public float radius;
	public LayerMask groundMask;
	public Transform SpawnPoint; //Where our player will be placed on respawn 
	public bool walking = false;
	public bool jumping = false;
	public bool running = false;
	public bool crouch = false;
	public bool dead = false;
	public bool block = false;
	public bool throwSpear = false;
	public bool isGrounded;
	public Camera myCamera; //We need to adjust camera on crouch, thus this.
	public Button PauseButton, runL, runR, jump, crouch_B, throw_; //Canvas Buttons: For the purpose of disabling and enabling them

	public Rigidbody2D projectile;
	public Transform projectileSpawnPoint;
	public float projectileVelocity;
	public int ArrowCount = 1;
	public GameObject[] Arrows;

	private bool showPopUp;
	private int dieTime = 3; //Delay seconds as the die animation plays then die
	private int direction = 1; //By default is facing right direction

	Rigidbody2D rb2D;
	int originalExtraJumps;
	float lastTapTime = 0;
	float tapSpeed = 0.5f; //in seconds

	//High Score is implemented, use "currentScore" in game logic to increment score that translates to highscore

	void Start()
	{
		//Show pop up message box only once on game startup
		//showPopUp = true;

		lastTapTime = 0; //Testing double tap
		throwSpear = false;
		currentScore = 0;
		highscore = PlayerPrefs.GetInt("High Score"); //Get saved Player High Score
		PlayerScore.text = "High Score: " + highscore.ToString();
	}

	void Awake()
	{
		rb2D = GetComponent<Rigidbody2D>(); // Our player's Rigidbody 2D
		originalExtraJumps = extraJumps;
		currentPlayerHealth = maxPlayerHealth;
		highscore = PlayerPrefs.GetInt("High Score"); //Get saved Player High Score
		PlayerScore.text = "High Score: " + highscore.ToString();
	}

	//Player collision calculations and actions
	void OnCollisionEnter2D(Collision2D player)
	{
		//If player collides with imaginary destroyer gameObject, kill that black man! ASAP!
		if (player.gameObject.name == "Destroy")
		{
			Debug.Log("You're dead");
			var buttons = FindObjectOfType<GameOverScript>();
			currentPlayerHealth = currentPlayerHealth - 10;

			StartCoroutine("DieAnimation");
			buttons.PlayerButtons(false);
			graphics.GetComponent<Animator>().SetBool("isDead", true); //die animation on
			playerFollow.transform.position = new Vector3(transform.position.x - .5f, transform.position.y, transform.position.z);
		}
	}

	void Update()
	{ //Update executes per frame

		highscore = highscore + currentScore;
		PlayerScore.text = "High Score: " + highscore.ToString();

		if (dieTime <= 0)
		{
			PlayerPrefs.SetInt("High Score", highscore); //Save highscore
			Die(); // We will have to work on this abit more by creating a respawn mechanism
		}

		// Reset player extrajumps to original on hitting ground
		if (isGrounded)//A bit buggy, needs more work
		{
			//Reset jumps back to original int value here
			extraJumps = resetJumps(extraJumps);
		}

		healthBar.fillAmount = (float)currentPlayerHealth / maxPlayerHealth; // This fills/unfills the healthbar
		var spearBehaviour = FindObjectOfType<SpearBehaviour>(); //We need to access the hasCollided boolean value to determine health damage
		if (spearBehaviour.hasCollided == true)
		{
			Debug.Log("Arrow has collided with player!");
			currentPlayerHealth = currentPlayerHealth - 2; //Deduct 2points everytime arrow hits player
		}


		//Just health interactive stuff
		//Color code references: https://docs.unity3d.com/ScriptReference/Color.html
		if (currentPlayerHealth <= 0)
		{
			HealthScore.text = "No Health!";
			StartCoroutine("DieAnimation");
			var buttons = FindObjectOfType<GameOverScript>();
			walking = false;
			crouch = false;
			running = false;
			jumping = false;
			block = false;
			buttons.PlayerButtons(false);
			graphics.GetComponent<Animator>().SetBool("isDead", true); //die animation on
			playerFollow.transform.position = new Vector3(transform.position.x - .5f, transform.position.y, transform.position.z);
		}

		else if (currentPlayerHealth <= 20)
		{
			HealthScore.color = new Color(1, 0, 0, 1); //Red
			HealthScore.text = "Danger: " + currentPlayerHealth.ToString();
		}

		else
		{
			HealthScore.color = new Color(1, 1, 1, 1); //White
			HealthScore.text = "Health: " + currentPlayerHealth.ToString();
		}
		// End of health interactive stuff

		if (walking)
		{
			Debug.Log("isMoving: " + walking);
			transform.Translate(Time.deltaTime * moveSpeed * direction, 0, 0, Camera.main.transform);
			isGrounded = Physics2D.OverlapCircle(groundPoint.position, radius, groundMask); //Returns true or false

			graphics.GetComponent<Animator>().SetBool("isMoving", walking); //Move animation on

		}

		else if (!walking)
		{
			//Debug.Log("isMoving: " + walking);
			graphics.GetComponent<Animator>().SetBool("isMoving", walking); //Move animation off
			isGrounded = Physics2D.OverlapCircle(groundPoint.position, radius, groundMask); //Returns true or false
		}

		if (running)
		{
			//Debug.Log("isRunning: " + running);
			transform.Translate(Time.deltaTime * runningSpeed * direction, 0, 0, Camera.main.transform);
			isGrounded = Physics2D.OverlapCircle(groundPoint.position, radius, groundMask); //Returns true or false

			graphics.GetComponent<Animator>().SetBool("isRunning", running); //Running animation on

		}

		else if (!running)
		{
			//Debug.Log("isRunning: " + running);
			graphics.GetComponent<Animator>().SetBool("isRunning", running); //Running animation off
		}



		if (crouch)
		{
			walking = false;
			running = false;
			block = false;
			jumping = false;
			graphics.GetComponent<Animator>().SetBool("isCrouching", crouch); //Crouch animation on

			//Here we take the player's current y position and minus for the y position change
			float crouch_position = gameObject.transform.position.y - .09f;
			float camera_crouch = myCamera.transform.position.y - .05f;

			//Obviously the player y (vertical) position will change slightly
			Vector3 crouching_pos = new Vector3(transform.position.x, crouch_position, transform.position.z); //:Bug in mobile, forces the player to the underworld due to collison problems.
			myCamera.transform.position = new Vector3(transform.position.x, camera_crouch, -10);

			gameObject.transform.position = crouching_pos;
		}

		else if (!crouch)
		{
			graphics.GetComponent<Animator>().SetBool("isCrouching", crouch); //Crouch animation off
		}

		if (block)
		{
			graphics.GetComponent<Animator>().SetBool("isBlocking", block);
			// implement invisible rigidbody block attached to player which has block tag so no health deductions
		}

		else if (!block)
		{
			//Disable invisible rigidbody block attached to player
			graphics.GetComponent<Animator>().SetBool("isBlocking", block);
		}

		if (isGrounded) //if player is on the ground then he's not in jumping state
		{
			jumping = false;
			graphics.GetComponent<Animator>().SetBool("isJump", jumping);
		}

		else if (!isGrounded) // if player is not on the ground then he's in the air so in jumping state
		{
			jumping = true;
			graphics.GetComponent<Animator>().SetBool("isJump", jumping);
		}

		if (throwSpear)
		{
			graphics.GetComponent<Animator>().SetBool("isThrowing", throwSpear); // Throw spear animation on

			//We need to find out if the throw animation is over then reset back to idle or curent player state
			if (graphics.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("throw")) 
			{
				//Debug.Log("Throw animation over");
				if(direction == 1)
				{
					projectile.GetComponent<SpriteRenderer>().flipX = true;
				}
				else
				{
					projectile.GetComponent<SpriteRenderer>().flipX = false;
				}
				Rigidbody2D spearInstance = Instantiate(projectile, projectileSpawnPoint.position, Quaternion.Euler(new Vector3(1, 0, transform.localEulerAngles.z))) as Rigidbody2D;
				spearInstance.name = "Spear("+ ArrowCount + ")";
				spearInstance.GetComponent<Rigidbody2D>().AddForce(projectileSpawnPoint.right * projectileVelocity);
				ArrowCount++;
				throwSpear = false;

				if (ArrowCount > 5)
				{
					//We need to destroy the arrows after we're done with them to save memory
					Arrows = GameObject.FindGameObjectsWithTag("player_spear");
					for (int i = 1; i < Arrows.Length; i++)
					{
						Destroy(Arrows[i]);
						Debug.Log("Cleaned: " + Arrows[i]);
					}
					ArrowCount = 1;
				}
			}
		}

		else if (!throwSpear)
		{
			graphics.GetComponent<Animator>().SetBool("isThrowing", throwSpear); // Throw spear animation off
		}

		//moveplayer(); //Move our player function: PC (uncomment when building for PC then comment the jumps & walking linked to UI buttons)


		//Touch mechnism, disable when working with UI touch. When enabled, interferes with UI button Click(L,R, Jump)
		//Our player phone touch control mechanism
		//if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Stationary)
		//{
		//    Vector2 touchPosition = Input.GetTouch(0).position;
		//    double halfScreen = Screen.width / 2.0;

		//    //Check if touch is on left or right?
		//    if (touchPosition.x < halfScreen)
		//    {
		//        gameObject.transform.Translate(Vector3.left * moveSpeed * Time.deltaTime); //Move left
		//        transform.localScale = new Vector3(-1, 1, 1); //Face Left direction
		//        graphics.GetComponent<Animator>().SetBool("isMoving", true); //Player movement animation
		//    }

		//    else if (touchPosition.x > halfScreen)
		//    {
		//        gameObject.transform.Translate(Vector3.right * moveSpeed * Time.deltaTime); //Move right
		//        transform.localScale = new Vector3(1, 1, 1); //Face right direction
		//        graphics.GetComponent<Animator>().SetBool("isMoving", true); //Player movement animation
		//    } 

		//    else
		//    {
		//        graphics.GetComponent<Animator>().SetBool("isMoving", false);
		//    }
		//}
	}

	IEnumerator DieAnimation()
	{
		while (true)
		{
			yield return new WaitForSeconds(1);
			dieTime--;
		}
	}

	//Pass current player jumps count/jump press and return new reset value
	int resetJumps(int currJumps)
	{
		if (currJumps < 1)
		{
			currJumps = 2;
		}
		return currJumps;
	}

	//Event Triggers for our player actions on button clicks

		// Jump
		public void onPointerDownJumping()
		{
			jumping = true;
		}
		public void onPointerUpJumping()
		{
			jumping = false;
		}

		// Move Right
		public void onPointerDownMove_R()
		{
			//Check if user double tapped on move button then set run motion
			if ((Time.time - lastTapTime) < tapSpeed)
			{
				Debug.Log("Double tap");
				running = true;
			}

			walking = true;

			lastTapTime = Time.time;

			direction = 1;
			transform.localScale = new Vector3(1, 1, 1); //Face Right direction
		
		}
		public void onPointerUpMove_R()
		{
			walking = false;
			running = false;
		}

		// Crouch
		public void onPointerDownCrouch()
		{
			crouch = true;
		}
		public void onPointerUpCrouch()
		{
			crouch = false;
		}

		// Move Left
		public void onPointerDownMove_L()
		{
			//Check if user double tapped on move button then set run motion
			if ((Time.time - lastTapTime) < tapSpeed)
			{
				//Debug.Log("Double tap");
				running = true;
			}

			walking = true;

			lastTapTime = Time.time;

			direction = -1;
			transform.localScale = new Vector3(-1, 1, 1); //Face Left direction 
		}
		public void onPointerUpMove_L()
		{
			walking = false;
			running = false;
		}  

		// Block
		public void onPointerDownBlock()
		{
			block = true;
		}
		public void onPointerUpBlock()
		{
			block = false;
		}		

		public void throwSpear_()
		{
			throwSpear = true;
	}

		
	//End of our Event Triggers

	void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(groundPoint.position, radius);
	}

	public void moveplayer()
	{
		Vector2 moveDir = new Vector2(Input.GetAxisRaw("Horizontal") * moveSpeed, rb2D.velocity.y);
		rb2D.velocity = moveDir;

		isGrounded = Physics2D.OverlapCircle(groundPoint.position, radius, groundMask); //Returns true or false

		if (Input.GetAxisRaw("Horizontal") == 1)
		{
			transform.localScale = new Vector3(1, 1, 1); //Face Right direction 
		}
		else if (Input.GetAxisRaw("Horizontal") == -1)
		{
			transform.localScale = new Vector3(-1, 1, 1); //Face Left direction 
		}

		if (Input.GetAxisRaw("Horizontal") != 0) // If player is moving, set moving animation
		{
			graphics.GetComponent<Animator>().SetBool("isMoving", true);
			//Debug.Log("is moving...");
		}
		else
		{
			graphics.GetComponent<Animator>().SetBool("isMoving", false);
			//Debug.Log("not moving...");
		}

		if (Input.GetKeyDown(KeyCode.Space))
		{ // If jump button is pressed
			Jump();
		}

		if (isGrounded)
		{
			//Debug.Log("On ground...");
			extraJumps = originalExtraJumps;
			graphics.GetComponent<Animator>().SetBool("isJump", false); //Set jump animation on hitting ground
			jumping = false;
		}
	}

	public void Jump() // Needs fixing, player flies instead of jumping
	{
		jump_click_count++;
		//Debug.Log("Jump...");
		if (extraJumps != 0 && isGrounded)
		{
			rb2D.AddForce(new Vector2(0, jumpHeight));
			extraJumps--;
		}

		else
		{
			jump_click_count = 0; // Reset back to 0 when not jumping
			rb2D.AddForce(new Vector2(0, 0));
		}

	}

	void Respawn()
	{
		//Set player to last known location...
		this.transform.position = SpawnPoint.transform.position;
	}


	public void Die()
	{
			StopCoroutine("DieAnimation"); //Die animation is over
			Popup.text = "Dead!";
			Popup.enabled = true;
			var gameOver = FindObjectOfType<GameOverScript>();
			var timeLeft_ = FindObjectOfType<Timer>();
			gameOver.dead = true;
			timeLeft_.PopUpText.enabled = true; //"Dead" text appears
			timeLeft_.StopCoroutine("LoseTime"); //Stop Timer
			//Debug.Log("Dead!");

		if (currentPlayerHealth <= 0 || dieTime <= 0)
			{
				graphics.gameObject.SetActive(false);
				//gameObject.GetComponentInChildren<Renderer>().enabled = false; //Hides the player (makes them invisible)
				gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll; //Freezes the player
				graphics.GetComponent<Animator>().SetBool("isMoving", false);
				graphics.GetComponent<Animator>().SetBool("isCrouching", false);
				graphics.GetComponent<Animator>().SetBool("isJump", false);
				graphics.GetComponent<Animator>().SetBool("isRunning", false);

				Vector3 startingPoint = new Vector3(SpawnPoint.position.x, SpawnPoint.position.y, SpawnPoint.position.z);
				gameObject.transform.position = startingPoint;
				gameOver.ShowButtons(); //Panel buttons
				gameOver.PlayerButtons(false); //Player action buttons
				//Debug.Log("Buttons disabled");

			}

			else
			{
				Respawn();
				var a = FindObjectOfType<GameOverScript>();
				a.HideButtons();
				gameOver.PlayerButtons(true);
			
			}
	}
		
	void OnGUI()
	{
		if (showPopUp)
		{
			GUI.Window(0, new Rect((Screen.width / 2) - 150, (Screen.height / 2) - 75
				   , 300, 180), ShowGUI, "A JOURNEY AWAITS YOU!");

		}
	}

	void ShowGUI(int windowID)
	{
		// You may put a label to show a message to the player
		GUI.Label(new Rect(60, 40, 200, 30), "Hello soldier, you are about to ");
		GUI.Label(new Rect(60, 60, 200, 30), "go on a journey of self discovery");
		GUI.Label(new Rect(60, 80, 200, 30), "Watch out for enemy fire, finish ");
		GUI.Label(new Rect(60, 100, 200, 40), "the intro terrain and get ready to fight.");

		// slot 1: moves L & R, slot 2: moves up & down, slot 3: +/- length, slot 4: +/- width
		if (GUI.Button(new Rect(120, 140, 75, 30), "OK"))
		{
			showPopUp = false;
		}

	}
}
