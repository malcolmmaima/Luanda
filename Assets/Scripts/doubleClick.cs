using UnityEngine;
using UnityEngine.UI;
using System.Collections;

//Refer to: https://www.youtube.com/watch?v=-ow-Dp17mYY
public class DoubleClick : MonoBehaviour {

	public Button button;
	private int counter;
	public float clickTimer = .5f;

	void start(){
		button.onClick.AddListener(buttonListener);
	}

	private void buttonListener(){
		counter++;
		if(counter == 1){
			StartCoroutine("doubleClickEvent");
		}
	}

	IEnumerator doubleClickEvent(){
		yield return new WaitForSeconds(clickTimer);
		if(counter > 1){
			Debug.Log("Double Click");
		}
		yield return new WaitForSeconds(.05f);
		counter = 0;
	}
}
